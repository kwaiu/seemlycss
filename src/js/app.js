$(document).ready(function(){
	//owl_init()
	//featherlight_init()
	hamburger()
})

function owl_init() {
	$('.owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		nav:true,
		items: 1,
		onInitialized: function(){
			$('.owl-dots button').each(function(index){
				$(this).attr('aria-label', 'Button ' + (index+1))
			})
			$('.owl-nav button').each(function(index){
				var arrow = 'Next'
				if(index == 0){ arrow = 'Back' }
				$(this).attr('aria-label', 'Owl Button ' + arrow)
			})
		}
	})
}

function featherlight_init() {
	$('a.fl').featherlight(
		{
			targetAttr: 'href', 
			afterOpen: function(){}
		}
	)
}

function hamburger() {
	
	var $hamburger = $(".hamburger")
	
	$hamburger.on("click", function(e) { 		
		$hamburger.toggleClass('is-active')
		$('body, #header').toggleClass('nav-panel')
		return false;
	})
}
