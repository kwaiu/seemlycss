# Seemly Css

## A responsive front-end framework with a small footprint

Seemly is a simple opensource CSS grid base framework intended to be a good start point for web projects. Seemly uses Sass and comes bundled with scripts to get you set up quickly so you can get Sassing!

## Features

- Responsive
- Mobile First
- Loads in a flash
- Simple to configure
- Flex box 
- Open source

## Get Started

Depending on your workflow there are a few ways to get Seemly.

## 1. Installing with package manager npm

Using npm 
`npm i seemlycss`

Using yarn 
`yarn add seemlycss`

## 2. Bitbucket
Clone or download the Seemly repository from bit bucket:

https://bitbucket.org/kwaiu/seemlycss

## 3. unpkg CDN
Or just reference the Seemly CSS via unpkg CDN:

https://unpkg.com/seemlycss/dist/css/app.min.css

Seemly was created using the CSS extension language Sass (Syntactically Awesome Stylesheets). 
To benefit from all it's great features use Node.js and GulpJS.


***
View the site https://seemlycss.com/


***
License [MIT](http://opensource.org/licenses/MIT)
