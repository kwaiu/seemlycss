'use strict';

/**
*	Required libraries for running Gulp tasks 
*/
const gulp = require('gulp');
const $ = require('gulp-load-plugins')({pattern: '*'});
const browserSync = require('browser-sync').create();

$.sass.compiler = require('node-sass');

/*
*	SassPaths for 3rd party SASS libraries. 
*	Let SASS know the directories it should look in when trying to import files. 
*	Correctly setting up the load paths allow the import of SASS libraries by name, 
*	rather than having using the relative path to the library. 
*/
let sassPaths = [
	'core/scss',
	'scss/settings/',
	'node_modules/normalize-scss/sass/',
	'node_modules/featherlight/release/',
	'node_modules/owl.carousel/src/scss/',
	'node_modules/hamburgers/_sass/hamburgers/',
	'node_modules/cookieconsent/build/'];

/*
*	Javascript files to include for exporting and uglifying to a single file
*/
let exportfiles = require('./exportfiles.json');

/* 
*	$ gulp serve 
*	start up browser-sync, 
*	watches out for changes to file locations 
*	calls specified sub functions 
*/
gulp.task('serve', function() {	
	browserSync.init({
		server: { baseDir: './' } 
	});
	
	gulp.watch('./core/**/*.scss', gulp.series('sass'));
	gulp.watch("./dist/css/*.css").on('change', browserSync.reload);
	
	gulp.watch("./dist/css/*.css").on('change', browserSync.reload);
	
	gulp.watch('./scss/**/*.scss', gulp.series('sass'));
	gulp.watch("./dist/css/*.css").on('change', browserSync.reload);
	
	gulp.watch('./src/js/app.js', gulp.series('project-js'));
	gulp.watch("./dist/js/**/*.js").on('change', browserSync.reload);

	gulp.watch("./index.html").on('change', browserSync.reload);
});

/* 
*	$ gulp sass 
*/
gulp.task('sass', function () {
	return gulp.src('./scss/**/*.scss')
	.pipe($.sourcemaps.init())
	.pipe($.plumber())
	.pipe($.sass({ includePaths: sassPaths, outputStyle: 'compact' }))	
	.pipe($.sass().on('error', $.sass.logError))
	.pipe($.sourcemaps.write())	
	.pipe(gulp.dest('./dist/css'))
	.pipe($.browserSync.stream())
	.pipe($.uglifycss({ "maxLineLen": 80, "uglyComments": true }))
	.pipe($.rename({ suffix: '.min' }))
	.pipe(gulp.dest('./dist/css'));	
});

/* 
*	$ gulp watch 
*/
gulp.task('watch', function () {
	gulp.watch('./scss/**/*.scss', gulp.series('sass'));
	gulp.watch('./src/js/app.js', gulp.series('project-js'));	
});

/* 
*	$ gulp project-js  
*	Uglifies the projects specific js and puts in specified destination  
*/
gulp.task('project-js', function (cb) {
	$.pump([
		gulp.src(['./src/js/app.js']),
		$.uglify(),
		$.rename({suffix: '.min'}),
		gulp.dest('./dist/js')
	],
	cb);
});

/* 
*	$ gulp export-js 
*	Export js files from exportfiles.json
*/
gulp.task('export-js', function() {	
	exportfiles = require('./exportfiles.json');
	return gulp.src(exportfiles.prodScripts)
	.pipe($.concat('export.js'))
	.pipe(gulp.dest('dist/js'));
});

/* 
*	$ gulp uglify-js 	
*/
gulp.task('uglify-js', function (cb) {  
	$.pump([
		gulp.src(['dist/js/export.js', 'src/js/app.js']),
		$.uglify(),
		$.rename({suffix: '.min'}),
		gulp.dest('dist/js')
	],
	cb);
});

exports.default = gulp.series(
	'sass',
	'project-js',
	'export-js',
	'uglify-js',
	'serve'
);

exports.build = gulp.series(
	'sass',
	'project-js',
	'export-js',
	'uglify-js'
);
